(* ----------------------------------------------------------------------------
 * Copyright (C) 2020, Université de Lille, Lille, FRANCE
 *
 * This file is part of WSymb.
 *
 * WSymb is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation ; either version 2 of
 * the License, or (at your option) any later version.
 *
 * WSymb is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY ; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program ; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *---------------------------------------------------------------------------- *)

let tool_name = "swymplify"
let usage = "Usage: "^tool_name^" <source-file>"
let version = "1.0.0"
          
let extension = ".pwf"

let debug = ref false
let to_c = ref false
let to_it = ref false
let to_py = ref false
let out_name = ref ""
         
let options = [
    "-p", Arg.Set to_py, "Compile a (single) formula to C code";
    "-c", Arg.Set to_c, "Compile a (single) formula to C code";
    "-i", Arg.Set to_it, "Compile in C with iterative computations (recursive otherwise)";
    "-debug", Arg.Set debug, "Run in debug mode";
    "-o", Arg.Set_string out_name, "Speficies the output file name";
    "-version", Arg.Unit (fun () -> print_endline version), "Print version"
  ]

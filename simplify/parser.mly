/* ---------------------------------------------------------------------------- */
/* Copyright (C) 2020, Université de Lille, Lille, FRANCE */

/* This file is part of WSymb. */

/* WSymb is free software; you can redistribute it and/or */
/* modify it under the terms of the GNU Lesser General Public License */
/* as published by the Free Software Foundation ; either version 2 of */
/* the License, or (at your option) any later version. */

/* WSymb is distributed in the hope that it will be useful, but */
/* WITHOUT ANY WARRANTY ; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU */
/* Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public */
/* License along with this program ; if not, write to the Free Software */
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 */
/* USA */
/* ---------------------------------------------------------------------------- */

%{
  open Utils
  open Symbol    
  open Loops
  open Abstract_wcet
  open Wcet_formula
%}

%token <string> IDENT
%token <int> INT
%token PIPE
%token BMULT
%token DOT
%token UNION PLUS BMINUS
%token CIRC
%token AND
%token EQUALS LEQUALS
%token TRUE FALSE
%token LPAR RPAR LCURLBRACKET RCURLBRACKET COMMA SCOL TOP INC PARAM BPARAM LOOP_ID
%token LOOPS ENDLH
                                                    
%token EOF

%start prog
%type <Context.t list> prog

%nonassoc COMMA CIRC SCOL

// Not sure who should have the highest priority
%left PLUS
%left UNION

%right DOT
%left PIPE
    
%%
                              
prog:
  context_list EOF { List.rev $1 }

context_list:
  context {[$1]}
| context_list context {$2::$1}

context:
formula hierarchy {
  let f,bl = $1 in
  Context.new_ctx f $2 (Loops.bounds_from_list bl)
}

hierarchy:
  LOOPS loop_inc_list ENDLH {$2}

loop_inc_list:
    {Loops.new_hierarchy ()}
    | loop_inc_list loop_inc {
          let inner,outers = $2 in
          Loops.add_inclusions $1 inner outers;
          $1
        }

loop_inc:
  loop_id INC loop_id_list SCOL { ($1, $3)}

loop_id_list:
  loop_id {[$1]}
| loop_id_list loop_id {$2::$1}

// Restrict to binary arity operators.
// Can't think of a simple solution to directly parse a full list of operands.    
formula:
    const { (FConst $1, []) }
  | param { (FParam $1, []) }
  | formula PLUS formula
    {
      let (f1,bl1),(f2,bl2) = $1,$3 in
      (FPlus [f1; f2], bl1@bl2)
    }
  | formula UNION formula
    {
      let (f1,bl1),(f2,bl2) = $1,$3 in
      (FUnion [f1; f2], bl1@bl2)
    }
  | LPAR formula COMMA formula COMMA loop_id RPAR CIRC sint
    {
      let (f1,bl1),(f2,bl2) = $2,$4 in
      (FPower (f1, f2, $6, $9), ($6,$9)::bl1@bl2)
    }
  | LPAR formula COMMA formula COMMA loop_id RPAR CIRC LPAR lexpression RPAR
    {
      let (f1,bl1),(f2,bl2) = $2,$4 in
      (FPowerParam (f1, f2, $6, $10), bl1@bl2)
    }
  | formula PIPE annot
    {
      let f, bl = $1 in
      (FAnnot (f, $3), bl)
    }
  | INT DOT formula
    {
      let f, bl = $3 in
      (FProduct ($1, f), bl)
    }
  | LPAR bexpressionlist RPAR BMULT formula
    {
      let f,bl = $5 in
      (FBProduct ($2,f), bl)
    }
  | LPAR formula RPAR {$2}
      
const:
LPAR loop_id SCOL LCURLBRACKET wcet_list RCURLBRACKET RPAR
{ let last = List.hd $5 in
  let wlist = List.rev (List.tl $5) in
  ($2, (wlist, last)) }

wcet_list:
INT {[$1]}
| wcet_list COMMA INT {$3::$1}

loop_id:
TOP {LTop}
| LOOP_ID INT { LNamed (string_of_int $2)}

annot:
LPAR loop_id COMMA INT RPAR { ($2, $4) }

sint:
INT {SInt $1}
| param { SParam $1}

param:
  PARAM INT { string_of_int $2 }

bexpressionlist:
  bexpression AND bexpressionlist
    {
      $1 :: $3
    }
  | bexpression
    {
      [$1]
    }

bexpression:
  INT LEQUALS lexpression
    {
      BLeq ($1,$3)
    }
  | INT EQUALS lexpression
    {
      BEq ($1,$3)
    }
  | TRUE
    {
      BBool true
    }
  | FALSE
    {
      BBool false
    }

lexpression:
  term PLUS lexpression
    {
       $1 :: $3
    }
  | term BMINUS lexpression
    {
      match $3 with
      | [] -> []
      | h :: t -> $1 :: ({coef = -h.coef; value = h.value} :: t)
    }
  | BMINUS term PLUS lexpression
    {
      let t = $2 in
      {coef = -t.coef; value = t.value} :: $4
    }
  | BMINUS term BMINUS lexpression
    {
      let t = $2 in
      match $4 with
      | [] -> {coef = -t.coef; value = t.value} :: $4
      | h :: hs -> {coef = -t.coef; value = t.value} :: ({coef = -h.coef; value = h.value} :: hs)
    }
  | BMINUS term
    {
      let t = $2 in
      [{coef = -t.coef; value = t.value}]
    }
  | term
    {
      [$1]
    } 

// represent a term
term:
  INT BMULT bparam
    {
      {coef = $1; value = (BParam $3)}
    }
  | INT
    {
      {coef = 1; value = (BConst $1)}
    }
  | bparam
    {
      {coef = 1; value = (BParam $1)}
    }

// complete boolean expression
//bexpression:
//  bexpression AND bexpression
//    {
//      BAnd [$1;$3]
//    }
//  | INT LEQUALS lexpression
//    {
//      BLeq ($1,$3)
//    }
//  | INT EQUALS lexpression
//    {
//      BEq ($1,$3)
//    }
//
//// right part of a boolean expression : the linear expression
//lexpression:
//  INT
//    {
//      BConst $1
//    }
//  | bparam
//    {
//      BParam $1
//    }
//  | lexpression PLUS lexpression
//    {
//      BPlus [$1;$3]
//    }
//  | lexpression BMINUS lexpression
//    {
//      BMinus [$1;$3]
//    }
//  | BMINUS lexpression
//    {
//      BMinus [BConst 0;$2]
//    } 

// definition of a function parameter
bparam:
  BPARAM INT { string_of_int $2 }
